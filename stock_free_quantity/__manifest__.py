# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Free Quantity",
    "version": "2.0.1.0.0",
    "author": "akretion,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "development_status": "Production/Stable",
    "category": "Warehouse",
    "depends": ["stock"],
    "license": "AGPL-3",
    "data": [
        "views/product_template_view.xml",
        "views/product_product_view.xml",
    ],
    "installable": True,
}
