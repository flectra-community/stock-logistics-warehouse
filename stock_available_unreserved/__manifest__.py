# Copyright 2018 Camptocamp SA
# Copyright 2016-19 ForgeFlow S.L. (https://www.forgeflow.com)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "Stock Available Unreserved",
    "summary": "Quantity of stock available for immediate use",
    "version": "2.0.1.1.0",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "development_status": "Production/Stable",
    "maintainers": ["LoisRForgeFlow"],
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "category": "Warehouse Management",
    "depends": ["stock"],
    "data": ["views/stock_quant_view.xml", "views/product_view.xml"],
    "license": "LGPL-3",
}
