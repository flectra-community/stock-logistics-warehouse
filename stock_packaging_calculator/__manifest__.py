# Copyright 2020 Camptocamp SA
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl)
{
    "name": "Stock packaging calculator",
    "summary": "Compute product quantity to pick by packaging",
    "version": "2.0.1.2.0",
    "development_status": "Alpha",
    "category": "Warehouse Management",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": ["product"],
}
