# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Vertical Lift - Server Environment",
    "summary": "Server Environment layer for Vertical Lift",
    "version": "2.0.1.0.0",
    "category": "Stock",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["stock_vertical_lift", "server_environment"],  # OCA/server-env
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "data": [],
    "installable": True,
    "auto_install": True,
    "development_status": "Alpha",
}
