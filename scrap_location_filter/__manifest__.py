# Copyright (C) 2021 Open Source Integrators
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Scrap Location Filter",
    "version": "2.0.1.0.0",
    "category": "Warehouse Management",
    "license": "AGPL-3",
    "depends": ["stock"],
    "summary": "Filters scrap location",
    "author": "Open Source Integrators, , Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "data": ["views/stock_views.xml"],
    "maintainers": ["opensourceintegrators"],
    "installable": True,
    "application": False,
}
