# Copyright 2021 Open Source Integrators
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).
{
    "name": "Stock Orderpoint Replenishment MRP demand origin details",
    "summary": "Link Purchase Orders to the replenishment demand MOs",
    "version": "2.0.1.0.0",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "category": "Warehouse",
    "depends": ["stock_orderpoint_origin", "purchase_mrp"],
    "installable": True,
    "auto_intall": True,
}
