# Copyright 2021 ForgeFlow S.L. (https://www.forgeflow.com)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "Stock Location Warehouse",
    "version": "2.0.1.0.0",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "summary": "Warehouse associated with a location",
    "category": "Stock Management",
    "depends": ["stock"],
    "data": ["views/stock_location.xml"],
    "installable": True,
    "development_status": "Alpha",
    "license": "LGPL-3",
}
