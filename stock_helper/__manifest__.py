# Copyright 2020-2021 Camptocamp SA
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Stock Helpers",
    "summary": "Add methods shared between various stock modules",
    "version": "2.0.1.0.0",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "category": "Hidden",
    "depends": ["stock"],
    "data": [],
    "installable": True,
    "license": "LGPL-3",
}
