# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl)
{
    "name": "Stock Move Common Destination",
    "summary": "Adds field for common destination moves",
    "version": "2.0.1.0.0",
    "category": "Warehouse Management",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["stock"],
    "data": ["views/stock_move.xml"],
}
