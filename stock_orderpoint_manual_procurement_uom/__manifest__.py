# Copyright 2018-20 ForgeFlow S.L. (https://www.forgeflow.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Stock Orderpoint Manual Procurement UoM",
    "summary": "Glue module for stock_orderpoint_uom and "
    "stock_orderpoint_manual_procurement",
    "version": "2.0.1.0.2",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "category": "Warehouse Management",
    "depends": ["stock_orderpoint_uom", "stock_orderpoint_manual_procurement"],
    "license": "AGPL-3",
    "installable": True,
    "application": False,
}
