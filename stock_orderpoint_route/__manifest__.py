# Copyright 2019 ForgeFlow, S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).
{
    "name": "Stock Orderpoint Route",
    "summary": "Allows to force a route to be used when procuring from orderpoints",
    "version": "2.0.1.1.1",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "ForgeFlow, Camptocamp, Odoo Community Association (OCA)",
    "category": "Warehouse",
    "depends": ["stock"],
    "data": ["views/stock_warehouse_orderpoint_views.xml"],
    "installable": True,
}
