# Flectra Community / stock-logistics-warehouse

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[stock_orderpoint_origin_mrp_link](stock_orderpoint_origin_mrp_link/) | 2.0.1.0.0| Link Purchase Orders to the replenishment demand MOs
[stock_request_picking_type](stock_request_picking_type/) | 2.0.1.0.0| Add Stock Requests to the Inventory App
[stock_packaging_calculator_packaging_type](stock_packaging_calculator_packaging_type/) | 2.0.1.0.0| Glue module for packaging type
[stock_location_warehouse](stock_location_warehouse/) | 2.0.1.0.0| Warehouse associated with a location
[stock_search_supplierinfo_code](stock_search_supplierinfo_code/) | 2.0.1.0.0|         Allows to search for picking from supplierinfo code
[stock_location_zone](stock_location_zone/) | 2.0.1.0.0| Classify locations with zones.
[stock_putaway_product_template](stock_putaway_product_template/) | 2.0.1.1.0| Add product template in putaway strategies from the product view
[stock_request_tier_validation](stock_request_tier_validation/) | 2.0.1.0.0| Extends the functionality of Stock Requests to support a tier validation process.
[stock_orderpoint_manual_procurement](stock_orderpoint_manual_procurement/) | 2.0.1.0.4| Allows to create procurement orders from orderpoints instead of relying only on the scheduler.
[stock_location_position](stock_location_position/) | 2.0.1.0.0| Add coordinate attributes on stock location.
[stock_cycle_count](stock_cycle_count/) | 2.0.1.2.0| Adds the capability to schedule cycle counts in a warehouse through different rules defined by the user.
[stock_move_common_dest](stock_move_common_dest/) | 2.0.1.0.0| Adds field for common destination moves
[stock_request_submit](stock_request_submit/) | 2.0.1.0.0| Add submit state on Stock Requests
[stock_location_bin_name](stock_location_bin_name/) | 2.0.1.0.0| Compute bin stock location name automatically
[stock_orderpoint_origin](stock_orderpoint_origin/) | 2.0.1.0.1| Link Purchase Orders to the replenishment demand Sales Orders
[stock_secondary_unit](stock_secondary_unit/) | 2.0.1.0.1| Get product quantities in a secondary unit
[scrap_reason_code](scrap_reason_code/) | 2.0.1.0.1| Reason code for scrapping
[stock_product_qty_by_packaging](stock_product_qty_by_packaging/) | 2.0.1.0.0| Compute product quantity to pick by packaging
[stock_request_purchase](stock_request_purchase/) | 2.0.1.0.0| Internal request for stock
[stock_request_kanban](stock_request_kanban/) | 2.0.1.0.0| Adds a stock request order, and takes stock requests as lines
[stock_location_empty](stock_location_empty/) | 2.0.1.0.0| Adds a filter for empty stock location
[stock_demand_estimate_matrix](stock_demand_estimate_matrix/) | 2.0.1.0.0| Allows to create demand estimates.
[stock_inventory_line_open](stock_inventory_line_open/) | 2.0.1.0.0| Open inventory lines on validated inventory adjustments
[stock_mts_mto_rule](stock_mts_mto_rule/) | 2.0.1.0.0| Add a MTS+MTO route
[stock_pull_list](stock_pull_list/) | 2.0.1.1.1| The pull list checks the stock situation and calculates needed quantities.
[stock_packaging_calculator](stock_packaging_calculator/) | 2.0.1.2.0| Compute product quantity to pick by packaging
[stock_demand_estimate](stock_demand_estimate/) | 2.0.1.1.0| Allows to create demand estimates.
[stock_inventory_cost_info](stock_inventory_cost_info/) | 2.0.1.0.0| Shows the cost of the inventory adjustments
[stock_available_mrp](stock_available_mrp/) | 2.0.1.0.5| Consider the production potential is available to promise
[scrap_location_filter](scrap_location_filter/) | 2.0.1.0.0| Filters scrap location
[stock_vertical_lift_qty_by_packaging](stock_vertical_lift_qty_by_packaging/) | 2.0.1.0.0|     Glue module for `stock_product_qty_by_packaging` and `stock_vertical_lift`.    
[stock_vertical_lift_packaging_type](stock_vertical_lift_packaging_type/) | 2.0.1.0.0| Provides integration with Vertical Lifts and packaging types
[stock_vertical_lift](stock_vertical_lift/) | 2.0.1.1.2| Provides the core for integration with Vertical Lifts
[stock_orderpoint_move_link](stock_orderpoint_move_link/) | 2.0.1.0.2| Link Reordering rules to stock moves
[stock_vertical_lift_kardex](stock_vertical_lift_kardex/) | 2.0.1.1.0| Integrate with Kardex Remstar Vertical Lifts
[procurement_auto_create_group](procurement_auto_create_group/) | 2.0.1.2.0| Allows to configure the system to propose automatically new procurement groups during the procurement run.
[stock_orderpoint_purchase_link](stock_orderpoint_purchase_link/) | 2.0.1.0.0| Link Reordering rules to purchase orders
[stock_location_children](stock_location_children/) | 2.0.1.0.0| Add relation between stock location and all its children
[stock_vertical_lift_empty_tray_check](stock_vertical_lift_empty_tray_check/) | 2.0.1.0.1| Checks if the tray is actually empty.
[stock_inventory_discrepancy](stock_inventory_discrepancy/) | 2.0.1.1.0| Adds the capability to show the discrepancy of every line in an inventory and to block the inventory validation when the discrepancy is over a user defined threshold.
[stock_measuring_device_zippcube](stock_measuring_device_zippcube/) | 2.0.1.0.1| Implement interface with Bosche Zippcube devicesfor packaging measurement
[stock_available_unreserved](stock_available_unreserved/) | 2.0.1.1.0| Quantity of stock available for immediate use
[stock_orderpoint_manual_procurement_uom](stock_orderpoint_manual_procurement_uom/) | 2.0.1.0.2| Glue module for stock_orderpoint_uom and stock_orderpoint_manual_procurement
[stock_generate_putaway_from_inventory](stock_generate_putaway_from_inventory/) | 2.0.1.0.0| Generate Putaway locations per Product deduced from Inventory
[stock_move_location](stock_move_location/) | 2.0.1.1.0| This module allows to move all stock in a stock location to an other one.
[stock_reserve_rule](stock_reserve_rule/) | 2.0.1.1.0| Configure reservation rules by location
[stock_warehouse_calendar](stock_warehouse_calendar/) | 2.0.1.0.1| Adds a calendar to the Warehouse
[stock_vertical_lift_storage_type](stock_vertical_lift_storage_type/) | 2.0.1.0.0| Compatibility layer for storage types on vertical lifts
[stock_request](stock_request/) | 2.0.1.0.7| Internal request for stock
[stock_change_qty_reason](stock_change_qty_reason/) | 2.0.1.0.2|         Stock Quantity Change Reason 
[stock_orderpoint_uom](stock_orderpoint_uom/) | 2.0.1.0.0| Allows to create procurement orders in the UoM indicated in the orderpoint
[stock_location_last_inventory_date](stock_location_last_inventory_date/) | 2.0.1.0.0| Show the last inventory date for a leaf location
[stock_available](stock_available/) | 2.0.1.0.3| Stock available to promise
[stock_measuring_device](stock_measuring_device/) | 2.0.1.0.1| Implement a common interface for measuring and weighing devices
[stock_move_auto_assign](stock_move_auto_assign/) | 2.0.1.0.0| Try to reserve moves when goods enter in a location
[stock_location_lockdown](stock_location_lockdown/) | 2.0.1.0.0| Prevent to add stock on locked locations
[stock_archive_constraint](stock_archive_constraint/) | 2.0.1.0.0| Stock archive constraint
[stock_picking_show_linked](stock_picking_show_linked/) | 2.0.1.0.1|        This addon allows to easily access related pickings       (in the case of chained routes) through a button       in the parent picking view.    
[stock_inventory_exclude_sublocation](stock_inventory_exclude_sublocation/) | 2.0.1.0.1| Allow to perform inventories of a location without including its child locations.
[stock_helper](stock_helper/) | 2.0.1.0.0| Add methods shared between various stock modules
[stock_exception](stock_exception/) | 2.0.1.0.1| Custom exceptions on stock picking
[stock_inventory_line_product_cost](stock_inventory_line_product_cost/) | 2.0.1.0.0| Stock Adjustment Cost
[stock_orderpoint_route](stock_orderpoint_route/) | 2.0.1.1.1| Allows to force a route to be used when procuring from orderpoints
[account_move_line_stock_info](account_move_line_stock_info/) | 2.0.1.0.1| Account Move Line Stock Info
[product_quantity_update_force_inventory](product_quantity_update_force_inventory/) | 2.0.1.0.1| Product Quantity Update Force Inventory
[stock_free_quantity](stock_free_quantity/) | 2.0.1.0.0| Stock Free Quantity
[stock_inventory_preparation_filter_pos](stock_inventory_preparation_filter_pos/) | 2.0.1.0.0| Add POS category filter on inventory adjustments
[stock_quant_manual_assign](stock_quant_manual_assign/) | 2.0.1.2.0| Stock - Manual Quant Assignment
[stock_request_direction](stock_request_direction/) | 2.0.1.0.1| From or to your warehouse?
[stock_location_tray](stock_location_tray/) | 2.0.1.1.2| Organize a location as a matrix of cells
[stock_vertical_lift_server_env](stock_vertical_lift_server_env/) | 2.0.1.0.0| Server Environment layer for Vertical Lift
[stock_putaway_method](stock_putaway_method/) | 2.0.1.0.0| Add the putaway strategy method back, removed from the stock module in Odoo 12
[account_move_line_product](account_move_line_product/) | 2.0.1.0.0| Displays the product in the journal entries and items
[stock_available_immediately](stock_available_immediately/) | 2.0.1.0.0| Ignore planned receptions in quantity available to promise
[stock_request_analytic](stock_request_analytic/) | 2.0.1.0.2| Internal request for stock
[stock_inventory_preparation_filter](stock_inventory_preparation_filter/) | 2.0.1.0.1| More filters for inventory adjustments
[stock_picking_cancel_confirm](stock_picking_cancel_confirm/) | 2.0.1.0.1| Stock Picking Cancel Confirm


