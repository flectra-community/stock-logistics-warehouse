# Copyright 2019 ForgeFlow, S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "Stock Orderpoint Move Link",
    "summary": "Link Reordering rules to stock moves",
    "version": "2.0.1.0.2",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "category": "Warehouse Management",
    "depends": ["stock"],
    "data": ["views/stock_move_views.xml"],
    "installable": True,
    "auto_install": False,
}
