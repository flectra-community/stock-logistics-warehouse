# Copyright 2018-19 ForgeFlow S.L. (https://www.forgeflow.com)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "Stock Warehouse Calendar",
    "summary": "Adds a calendar to the Warehouse",
    "version": "2.0.1.0.1",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/stock-logistics-warehouse",
    "author": "ForgeFlow, " "Odoo Community Association (OCA)",
    "category": "Warehouse Management",
    "depends": ["stock", "resource"],
    "data": ["views/stock_warehouse_views.xml"],
    "installable": True,
    "development_status": "Production/Stable",
    "maintainers": ["JordiBForgeFlow"],
}
